﻿using CinemaTicketsBookingClient.CinemaService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaTicketsBookingClient
{
    class Program
    {
        public CinemaServiceClient serviceClient;
        public User user;
        public List<Movie> movies;
        List<Reservation> usersReservations;

        public Program()
        {
            serviceClient = new CinemaServiceClient("BasicHttpBinding_ICinemaService");
        }

        static void Main(string[] args)
        {
            Program program = new Program();

            do
            {
                string input = Console.ReadLine();
                string[] words = input.Split(' ');

                switch (words[0])
                {
                    case "reservations":
                        Console.WriteLine(program.GetReservations());
                        break;
                    case "updateReservation":
                        try
                        {
                            Console.WriteLine(program.UpdateReservation(Int32.Parse(words[1]), Int32.Parse(words[2]), Int32.Parse(words[3])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "movies":
                        Console.WriteLine(program.GetMovies());
                        break;
                    case "login":
                        try
                        {
                            Console.WriteLine(program.Login(words[1], words[2]));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "user":
                        if (program.user != null)
                            Console.WriteLine(program.user.Login);
                        else
                            Console.WriteLine("Nobody is logged in");
                        break;
                    case "deleteReservation":
                        try
                        {
                            Console.WriteLine(program.deleteReservation(Int32.Parse(words[1])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "reserve":
                        try
                        {
                            Console.WriteLine(program.Reserve(Int32.Parse(words[1]), Int32.Parse(words[2])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "exit":
                        program.serviceClient.Close();
                        Environment.Exit(0);
                        break;
                    case "help":
                        Console.WriteLine(availableCommands());
                        break;
                    case "logout":
                        program.Logout();
                        break;
                    default:
                        Console.WriteLine(availableCommands());
                        break;
                }

            } while (true);
        }

        public static string availableCommands()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Available commands : ");
            sb.AppendLine(" - 'login yourLogin yourPassword'                               -  To login use ");
            sb.AppendLine(" - 'movies'                                                     -  To check avaiable movies use ");
            sb.AppendLine(" - 'reserve movieNumber seatNumber'                             z-  To book a seat use ");
            sb.AppendLine(" - 'reservations'                                               -  To check your reservations use  ");
            sb.AppendLine(" - 'user'                                                       -  To check logged user login use  ");
            sb.AppendLine(" - 'updateReservation reservationNumber movieNumber seatNumber' -  To update reservation use  ");
            sb.AppendLine(" - 'deleteReservation reservationNumber'                        -  To delete reservation use   ");
            sb.AppendLine(" - 'logout'                                                     -  To logout use ");
            sb.AppendLine(" - 'exit'                                                       -  To exit program use  ");
            return sb.ToString();
        }
        public string GetMovies()
        {

            try
            {
                movies = serviceClient.GetMovies();
            }
            catch (Exception)
            {

               return "Problem with connection to the service";
            }
           

            StringBuilder sb = new StringBuilder();
            int i = 1;
            foreach (Movie movie in movies)
            {
                sb.AppendLine();
                sb.AppendLine(String.Format("{0} \"{1}\" {2} {3} ", i, movie.Title, movie.EmissionTime.Date.ToShortDateString(), movie.EmissionTime.ToShortTimeString()));
                sb.AppendLine("Free seats :");
                sb.AppendLine("");
                foreach (Seat seat in movie.Seats)
                {
                    if (seat.Booked == false)
                        sb.Append(String.Format(" {0} ", seat.SeatNumber));
                }
                sb.AppendLine();
                i++;
            }
            sb.AppendLine();
            return sb.ToString();
        }
        public string Login(string login, string password)
        {
            User tmpUser = new User { Login = login, Password = password };
            try
            {
                user = serviceClient.Login(tmpUser);
            }
            catch (Exception)
            {

                return "Problem with connection to the service";
            }
           
            if (user == null)
                return "Login or password doesnt match";
            else
                return "hello " + user.Name;
        }
        public string Reserve(int movieNumber, int seatNumber)
        {
            if (user == null)
                return "You must log in !";

            try
            {
                movies = serviceClient.GetMovies();
            }
            catch (Exception)
            {

                return "Problem with connection to the service";
            }

           


            Movie movie;
            try
            {
                movie = movies.ElementAt(movieNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong movie number !";
            }
            if (movie == null)
                return "Wrong title !";
            var seat = movie.Seats.Where(x => x.SeatNumber == seatNumber && x.Booked == false).FirstOrDefault();
            if (seat == null)
                return "Wrong seat !";
            else
                seat = serviceClient.SetSeatBooked(seat);

            if (serviceClient.CreateReservation(movie, user, seat))
            {
                return "Reservation successful";
            }
            else
            {
                return "Something gone wrong";
            }
        }
        public string GetReservations()
        {
            if (user == null)
                return "You must log in !";
            usersReservations = serviceClient.GetReservations(user);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" Reservations for  " + user.Name);

            int i = 1;
            foreach (Reservation reserv in usersReservations)
            {
                sb.AppendLine(String.Format(" {0} \"{1}\" {2} {3} {4} ", i, reserv.Movie.Title, reserv.Movie.EmissionTime.Date.ToShortDateString(), reserv.Movie.EmissionTime.ToShortTimeString(), reserv.Seat.SeatNumber));
                i++;
            }
            return sb.ToString();

        }
        public void Logout()
        {
            user = null;
            usersReservations = null;
        }
        public string UpdateReservation(int reservNumber, int movieNumber, int seatNumber)
        {
            if (user == null)
                return "You must log in !";
            movies = serviceClient.GetMovies();
            usersReservations = serviceClient.GetReservations(user);
            Movie movie;
            Reservation res;
            try
            {
                movie = movies.ElementAt(movieNumber - 1);
                res = usersReservations.ElementAt(reservNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong movie or reservation number !";
            }
            var seat = movie.Seats.Where(x => x.SeatNumber == seatNumber && x.Booked == false).FirstOrDefault();
            if (seat == null)
                return "Wrong seat !";
            res.Movie = movie;
            res.Seat = seat;
            if (serviceClient.UpdateReservation(res))
                return "Reservations updated succesfully";
            else
                return "Something gone wrong";
        }
        public string deleteReservation(int reservNumber)
        {
            if (user == null)
                return "You must log in !";
            usersReservations = serviceClient.GetReservations(user);
            Reservation res;
            try
            {
                res = usersReservations.ElementAt(reservNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong reservation number !";
            }
            if (serviceClient.DeleteReservation(res))
                return "Reservations deleted succesfully";
            else
                return "Something gone wrong";
        }
        //update zrobic że podaje numer rezerwacji i numer miejsca, usuwania - numer z listy czy obiekt?
    }
}
