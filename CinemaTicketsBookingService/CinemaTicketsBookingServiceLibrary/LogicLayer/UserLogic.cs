﻿using DataLayer;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer
{
    public class UserLogic
    {
        private Context context;
        public UserLogic()
        {
            context = new Context();
        }

        public User Login(User user)
        {
            try
            {
                return context.Users.Where(x => x.Login == user.Login && x.Password == user.Password).FirstOrDefault();
                    
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
