﻿using DataLayer;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace LogicLayer
{
    public class ReservationLogic
    {
        private Context context;       
        

        public bool CreateReservation(User user, Movie movie, Seat seat)
        {
            context = new Context();
            try
            {           
                var tmpmovie = context.Movies.Where(x => x.ID == movie.ID).FirstOrDefault();
                var tmpSeat = context.Seats.Where(x => x.ID == seat.ID).FirstOrDefault();
                var tmpu = context.Users.Where(x => x.ID == user.ID).FirstOrDefault();
                context.Reservations.Add(new Reservation { Movie = tmpmovie, Seat = tmpSeat, User = tmpu });                
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        

        public bool UpdateReservation(Reservation reservation)
        {
            context = new Context();
            try
            {
                var res = context.Reservations.Include("Seat").Where(x => x.ID == reservation.ID).FirstOrDefault();
                res.Seat.Booked = false;

                res.Movie = context.Movies.Where(x => x.ID == reservation.Movie.ID).FirstOrDefault();
                var dbSeat = context.Seats.Where(x => x.ID == reservation.Seat.ID).FirstOrDefault();
                dbSeat.Booked = true;
                res.Seat = dbSeat;
                context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteReservation(int reservationId)
        {
            context = new Context();
            try
            {
                var seatId = context.Reservations.Where(x => x.ID == reservationId).FirstOrDefault().Seat.ID;
                context.Reservations.Remove(context.Reservations.Where(x => x.ID == reservationId).First());
                context.Seats.Where(x => x.ID == seatId).FirstOrDefault().Booked = false;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool DeleteReservation(Reservation reservation)
        {
            context = new Context();
            try
            {
                context.Reservations.Remove(context.Reservations.Where(x => x.ID == reservation.ID).First());
                context.Seats.Where(x => x.ID == reservation.Seat.ID).FirstOrDefault().Booked = false;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Reservation> GetUserReservations(User user)
        {
            context = new Context();
            try
            {
                var tmp = context.Movies.Include("Seats").ToList();
                var tmp2 = context.Reservations.Include("Movie").Include("Seat").Include("User").ToList();
              
                List<Reservation> c = context.Reservations.Where(x => x.User.ID == user.ID).ToList();

                foreach (var item in c)
                {
                    item.Movie.Seats = null;
                    item.User.Reservations = null;

                }

                return c;
            }
            catch (Exception ex)
            {
                return new List<Reservation>();
            }
        }
        public List<Reservation> GetUserReservations(int userID)
        {
            context = new Context();
            try
            {
                var tmp = context.Movies.Include("Seat").ToList();
                var tmp2 = context.Reservations.Include("Movie").ToList();                
                return context.Reservations.Where(x => x.User.ID == userID).ToList();
            }
            catch (Exception)
            {
                return new List<Reservation>();
            }
        }
    }
}
