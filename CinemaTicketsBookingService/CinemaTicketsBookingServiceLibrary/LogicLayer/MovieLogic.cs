﻿using DataLayer;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
namespace LogicLayer
{
    public class MovieLogic
    {
        private Context context;

        public List<Movie> GetMovies()
        {
            context = new Context();
            try
            {
                return context.Movies.Include("Seats").ToList();
            }
            catch (Exception ex)
            {
                return new List<Movie>();
            }
        }
    }
}
