﻿using DataLayer;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer
{
    public class SeatLogic
    {
        private Context context;
        public SeatLogic()
        {
            context = new Context();
        }

        public Seat SetBooked(Seat seat)
        {
            context = new Context();
            try
            {
                var dbSeat = context.Seats.Where(x => x.ID == seat.ID).FirstOrDefault();
                dbSeat.Booked = true;
                context.SaveChanges();
                return dbSeat;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<Seat> GetSeats()
        {
            context = new Context();
            try
            {
                return context.Seats.ToList();
            }
            catch (Exception)
            {
                return new List<Seat>();
            }
        }
    }
}
