﻿using DataModel.Models;
using LogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(CinemaTicketsBookingServiceLibrary.CinemaService));
            try
            {
                host.Open();
                printServiceInfo(host);
                Console.ReadLine();
                host.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                host.Abort();
            }

            
        }

        static void printServiceInfo(ServiceHost host)
        {
            Console.WriteLine("{0} is up and running with theses endpoints : ",host.Description.ServiceType);

            foreach (ServiceEndpoint endpoint in host.Description.Endpoints)
            {
                Console.WriteLine(endpoint.Address);
            }
        }
        public static void CreateDB()
        {
            //ReservationLogic Logic = new ReservationLogic();
            //MovieLogic movieLogic = new MovieLogic();
            //SeatLogic seatLogic = new SeatLogic();

            //foreach (Movie item in movieLogic.GetMovies())
            //{
            //    Console.WriteLine(item.Title);
            //    foreach (Seat seat in item.Seats)
            //    {
            //        Console.WriteLine(seat.ID);
            //    }
            //}
            //Console.WriteLine("SEATS :");

            //foreach (Seat item in seatLogic.GetSeats())
            //{
            //    Console.WriteLine(item.ID);
            //}
        }
    }
}
