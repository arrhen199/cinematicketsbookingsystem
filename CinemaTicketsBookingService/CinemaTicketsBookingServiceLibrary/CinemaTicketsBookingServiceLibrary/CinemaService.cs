﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using DataModel.Models;
using LogicLayer;

namespace CinemaTicketsBookingServiceLibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class CinemaService : ICinemaService
    {
        private ReservationLogic reservationLogic;
        private MovieLogic movieLogic;
        private UserLogic userLogic;
        private SeatLogic seatLogic;

        public CinemaService()
        {
            reservationLogic = new ReservationLogic();
            movieLogic = new MovieLogic();
            userLogic = new UserLogic();
            seatLogic = new SeatLogic();
        }

        public bool CreateReservation(Movie movie, User user, Seat seat)
        {
            return reservationLogic.CreateReservation(user, movie, seat);
        }

        public bool DeleteReservation(Reservation reservation)
        {
            return reservationLogic.DeleteReservation(reservation);
        }

        public List<Movie> GetMovies()
        {
            return movieLogic.GetMovies();
        }

        public List<Reservation> GetReservations(User user)
        {
            return reservationLogic.GetUserReservations(user);
        }

        public User Login(User user)
        {
            return userLogic.Login(user);
        }

        public bool UpdateReservation(Reservation reservation)
        {
            return reservationLogic.UpdateReservation(reservation);
        }

        public Seat SetSeatBooked(Seat seat)
        {
            return seatLogic.SetBooked(seat);
        }
    }
}
