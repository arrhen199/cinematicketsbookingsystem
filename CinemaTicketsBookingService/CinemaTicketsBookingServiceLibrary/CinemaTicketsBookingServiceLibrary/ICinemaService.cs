﻿using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CinemaTicketsBookingServiceLibrary
{
    [ServiceContract]
    public interface ICinemaService
    {
        [OperationContract]
        List<Movie> GetMovies();
        [OperationContract]
        List<Reservation> GetReservations(User user);

        [OperationContract]
        bool CreateReservation(Movie movie, User user, Seat seat);
        [OperationContract]
        bool UpdateReservation(Reservation reservation);
        [OperationContract]
        bool DeleteReservation(Reservation reservation);

        [OperationContract]
        User Login(User user);

        [OperationContract]
        Seat SetSeatBooked(Seat seat);

    }
}
