﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    [DataContract]
    public class Movie
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime EmissionTime { get; set; }

        [DataMember]
        public virtual List<Seat> Seats { get; set; }
        
    }
}
