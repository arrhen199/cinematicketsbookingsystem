﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    [DataContract]
    public class Seat
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public bool Booked { get; set; }
        [DataMember]
        public int SeatNumber { get; set; }
    }
}
