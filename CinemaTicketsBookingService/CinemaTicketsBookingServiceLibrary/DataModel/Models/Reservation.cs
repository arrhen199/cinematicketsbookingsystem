﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    [DataContract]
    public class Reservation
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public virtual User User { get; set; }
        [DataMember]
        public virtual Movie Movie { get; set; }
        [DataMember]
        public virtual Seat Seat { get; set; }
    }
}
