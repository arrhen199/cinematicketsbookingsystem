﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataModel.Models;

namespace DataLayer
{
    public class Context : DbContext
    {
        //public Context() : base("Context") { }
        public Context() : base("Context")
        {
            Database.SetInitializer<Context>(new CinemaTicketsDbInitializer());
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<User> Users{ get; set; }
        public DbSet<Reservation> Reservations { get; set; }

    }
    public class CinemaTicketsDbInitializer : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {            
            Movie movie1 = new Movie { Title = "Jak cheatować w cs go", EmissionTime = new DateTime(2016, 7, 21, 20, 45, 00) };
            Movie movie2 = new Movie { Title = "Embargo", EmissionTime = new DateTime(2016, 7, 22, 15, 45, 00) };

            movie1.Seats = new List<Seat>();
            movie2.Seats = new List<Seat>();

            for (int i = 1; i < 21; i++)
            {
                movie1.Seats.Add(new Seat { Booked = false, SeatNumber = i });
                movie2.Seats.Add(new Seat { Booked = false, SeatNumber = i });
            }
            context.Movies.Add(movie1);
            context.Movies.Add(movie2);

            User user = new User { Login = "rsi", Name = "Alina", Password = "rsi" };
            context.Users.Add(user);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
